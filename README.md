# styled-react-modal

Link to [npm-package](https://www.npmjs.com/package/@cortopratt/styled-react-modal)

    Simple modal built with styled-components

## Installation

- install [styled-components (**version ^5.3.3**)](https://styled-components.com/)

- add [fontawesome](https://fontawesome.com/) cdn in your head index.html file

## Example for styling modal

Modal component contain default styles which can be customized via props.

The details of properties below example.

```jsx
import React from "react";
import styled from "styled-components";
import { Modal } from "@cortopratt/styled-react-modal";

const MyComponent = () => {
  return (
    <Modal
      backgroundColor="white"
      ModalContentColor="green"
      // ... other properties
    >
      Some content !
    </Modal>
  );
};
```

## Properties for styling modal

- Background
  - backgroundColor: backgound-color
- Container
  - ModalWrapperBorderRadius: border-radius
  - ModalWrapperBorder: border
  - ModalWrapperWidth: width
  - ModalWrapperMaxWidth: max-width
  - ModalWrapperHeight: height
  - ModalWrapperBoxShadow: box-shadow
  - ModalWrapperBackground: background
  - ModalWrapperColor: color
- Content (text ...)
  - ModalContentPaddingTop: padding-top
  - ModalContentPaddingBottom: padding-bottom
  - ModalContentPaddingRight: padding-right
  - ModalContentPaddingLeft: padding-left
  - ModalContentLineHeight: line-height
  - ModalContentFontWeight: font-weight
  - ModalContentFontSize: font-size
  - ModalContentColor: color
- Close button
  - CloseBtnColor: color
  - CloseBtnTop: top
  - CloseBtnRight: right
  - CloseBtnWidth: width
  - CloseBtnHeight: height

## Example for handle open/close modal

Just add showModal and setShowModal props as shown in modal props.

```jsx
import React from "react";
import styled from "styled-components";
import { Modal } from "@cortopratt/styled-react-modal";

const App = () => {
  const [showModal, setShowModal] = useState(false);
  return (
    <Modal
      // to handle if modal is open or not
      showModal={showModal}
      // to handle close modal button
      setShowModal={setShowModal}
    >
      Some content !
    </Modal>
    // example with a simple button.
    <button onClick={() => setShowModal(true)}>
      Open Modal
    </button>
    // example with passing prop to form validation
    <Form setShowModal={setShowModal} />
  );
};
```
