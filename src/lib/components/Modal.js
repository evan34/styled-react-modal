import React from 'react';
import styled from 'styled-components';

const Background = styled.div`
  position: fixed;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
  z-index: 9;
  background-color: ${(props) => props.backgroundColor || 'rgba(0, 0, 0, 0.8)'};
`;

const ModalWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: relative;
  z-index: 10;
  margin: 0 auto;
  transition: all 0.3s ease-out;
  border-radius: ${(props) => props.ModalWrapperBorderRadius || '10px'};
  border: ${(props) => props.ModalWrapperBorder || '2px solid #32be3f'}; 
  width: ${(props) => props.ModalWrapperWidth || 'auto'};
  max-width: ${(props) => props.ModalWrapperMaxWidth || '50%'};
  height: ${(props) => props.ModalWrapperHeight || 'auto'};
  box-shadow: ${(props) => props.ModalWrapperBoxShadow || '0 5px 16px rgba(0, 0, 0, 0.2)'}; 
  background: ${(props) => props.ModalWrapperBackground || '#fff'}; 
  color: ${(props) => props.ModalWrapperColor || '#000'}; 
`;

const ModalContent = styled.p`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  transition: all 0.3s ease-out;
  padding-top: ${(props) => props.ModalContentPaddingTop || '10px'};
  padding-bottom: ${(props) => props.ModalContentPaddingBottom || '10px'};
  padding-right: ${(props) => props.ModalContentPaddingRight || '20px'};
  padding-left: ${(props) => props.ModalContentPaddingLeft || '20px'};
  line-height: ${(props) => props.ModalContentLineHeight || '1.8'};
  font-weight: ${(props) => props.ModalContentFontWeight || '500'};
  font-size: ${(props) => props.ModalContentFontSize || '20px'};
  color: ${(props) => props.ModalContentColor || '#32be3f'};
`;

const CloseModalButton = styled.i`
  cursor: pointer;
  position: absolute;
  padding: 0;
  z-index: 10;
  display: flex;
  justify-content: center;
  align-items: center;
  color: ${(props) => props.CloseBtnColor || '#5f5d5d'};
  top: ${(props) => props.CloseBtnTop || '5px'};
  right: ${(props) => props.CloseBtnRight || '5px'};
  width: ${(props) => props.CloseBtnWidth || '20px'};
  height: ${(props) => props.CloseBtnHeight || '20px'};
`;

const Modal = ({
  showModal,
  setShowModal,
  children,
  backgroundColor,
  ModalWrapperBorderRadius,
  ModalWrapperBorder,
  ModalWrapperWidth,
  ModalWrapperMaxWidth,
  ModalWrapperHeight,
  ModalWrapperBoxShadow,
  ModalWrapperBackground,
  ModalWrapperColor,
  ModalContentPaddingTop,
  ModalContentPaddingBottom,
  ModalContentPaddingRight,
  ModalContentPaddingLeft,
  ModalContentLineHeight,
  ModalContentFontWeight,
  ModalContentFontSize,
  ModalContentColor,
  CloseBtnColor,
  CloseBtnTop,
  CloseBtnRight,
  CloseBtnWidth,
  CloseBtnHeight,
}) => (
  <>
    {
    showModal ? (
      <Background
        backgroundColor={backgroundColor}
        style={{
          transform: showModal ? 'translateY(0vh)' : 'translateY(-100vh)',
          opacity: showModal ? '1' : '0',
        }}
      >
        <ModalWrapper
          ModalWrapperBorderRadius={ModalWrapperBorderRadius}
          ModalWrapperBorder={ModalWrapperBorder}
          ModalWrapperWidth={ModalWrapperWidth}
          ModalWrapperMaxWidth={ModalWrapperMaxWidth}
          ModalWrapperHeight={ModalWrapperHeight}
          ModalWrapperBoxShadow={ModalWrapperBoxShadow}
          ModalWrapperBackground={ModalWrapperBackground}
          ModalWrapperColor={ModalWrapperColor}
        >
          <CloseModalButton
            className="fas fa-times"
            onClick={() => setShowModal((isShowing) => !isShowing)}
            CloseBtnColor={CloseBtnColor}
            CloseBtnTop={CloseBtnTop}
            CloseBtnRight={CloseBtnRight}
            CloseBtnWidth={CloseBtnWidth}
            CloseBtnHeight={CloseBtnHeight}
          />
          <ModalContent
            ModalContentPaddingTop={ModalContentPaddingTop}
            ModalContentPaddingBottom={ModalContentPaddingBottom}
            ModalContentPaddingRight={ModalContentPaddingRight}
            ModalContentPaddingLeft={ModalContentPaddingLeft}
            ModalContentLineHeight={ModalContentLineHeight}
            ModalContentFontWeight={ModalContentFontWeight}
            ModalContentFontSize={ModalContentFontSize}
            ModalContentColor={ModalContentColor}
          >
            { children }
          </ModalContent>
        </ModalWrapper>
      </Background>
    )
      : null
    }
  </>
);

export default Modal;
