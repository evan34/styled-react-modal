"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject, _templateObject2, _templateObject3, _templateObject4;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

const Background = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  position: fixed;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 100vw;\n  height: 100vh;\n  z-index: 9;\n  background-color: ", ";\n"])), props => props.backgroundColor || 'rgba(0, 0, 0, 0.8)');

const ModalWrapper = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  position: relative;\n  z-index: 10;\n  margin: 0 auto;\n  transition: all 0.3s ease-out;\n  border-radius: ", ";\n  border: ", "; \n  width: ", ";\n  max-width: ", ";\n  height: ", ";\n  box-shadow: ", "; \n  background: ", "; \n  color: ", "; \n"])), props => props.ModalWrapperBorderRadius || '10px', props => props.ModalWrapperBorder || '2px solid #32be3f', props => props.ModalWrapperWidth || 'auto', props => props.ModalWrapperMaxWidth || '50%', props => props.ModalWrapperHeight || 'auto', props => props.ModalWrapperBoxShadow || '0 5px 16px rgba(0, 0, 0, 0.2)', props => props.ModalWrapperBackground || '#fff', props => props.ModalWrapperColor || '#000');

const ModalContent = _styledComponents.default.p(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  transition: all 0.3s ease-out;\n  padding-top: ", ";\n  padding-bottom: ", ";\n  padding-right: ", ";\n  padding-left: ", ";\n  line-height: ", ";\n  font-weight: ", ";\n  font-size: ", ";\n  color: ", ";\n"])), props => props.ModalContentPaddingTop || '10px', props => props.ModalContentPaddingBottom || '10px', props => props.ModalContentPaddingRight || '20px', props => props.ModalContentPaddingLeft || '20px', props => props.ModalContentLineHeight || '1.8', props => props.ModalContentFontWeight || '500', props => props.ModalContentFontSize || '20px', props => props.ModalContentColor || '#32be3f');

const CloseModalButton = _styledComponents.default.i(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n  cursor: pointer;\n  position: absolute;\n  padding: 0;\n  z-index: 10;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  color: ", ";\n  top: ", ";\n  right: ", ";\n  width: ", ";\n  height: ", ";\n"])), props => props.CloseBtnColor || '#5f5d5d', props => props.CloseBtnTop || '5px', props => props.CloseBtnRight || '5px', props => props.CloseBtnWidth || '20px', props => props.CloseBtnHeight || '20px');

const Modal = _ref => {
  let {
    showModal,
    setShowModal,
    children,
    backgroundColor,
    ModalWrapperBorderRadius,
    ModalWrapperBorder,
    ModalWrapperWidth,
    ModalWrapperMaxWidth,
    ModalWrapperHeight,
    ModalWrapperBoxShadow,
    ModalWrapperBackground,
    ModalWrapperColor,
    ModalContentPaddingTop,
    ModalContentPaddingBottom,
    ModalContentPaddingRight,
    ModalContentPaddingLeft,
    ModalContentLineHeight,
    ModalContentFontWeight,
    ModalContentFontSize,
    ModalContentColor,
    CloseBtnColor,
    CloseBtnTop,
    CloseBtnRight,
    CloseBtnWidth,
    CloseBtnHeight
  } = _ref;
  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, showModal ? /*#__PURE__*/_react.default.createElement(Background, {
    backgroundColor: backgroundColor,
    style: {
      transform: showModal ? 'translateY(0vh)' : 'translateY(-100vh)',
      opacity: showModal ? '1' : '0'
    }
  }, /*#__PURE__*/_react.default.createElement(ModalWrapper, {
    ModalWrapperBorderRadius: ModalWrapperBorderRadius,
    ModalWrapperBorder: ModalWrapperBorder,
    ModalWrapperWidth: ModalWrapperWidth,
    ModalWrapperMaxWidth: ModalWrapperMaxWidth,
    ModalWrapperHeight: ModalWrapperHeight,
    ModalWrapperBoxShadow: ModalWrapperBoxShadow,
    ModalWrapperBackground: ModalWrapperBackground,
    ModalWrapperColor: ModalWrapperColor
  }, /*#__PURE__*/_react.default.createElement(CloseModalButton, {
    className: "fas fa-times",
    onClick: () => setShowModal(isShowing => !isShowing),
    CloseBtnColor: CloseBtnColor,
    CloseBtnTop: CloseBtnTop,
    CloseBtnRight: CloseBtnRight,
    CloseBtnWidth: CloseBtnWidth,
    CloseBtnHeight: CloseBtnHeight
  }), /*#__PURE__*/_react.default.createElement(ModalContent, {
    ModalContentPaddingTop: ModalContentPaddingTop,
    ModalContentPaddingBottom: ModalContentPaddingBottom,
    ModalContentPaddingRight: ModalContentPaddingRight,
    ModalContentPaddingLeft: ModalContentPaddingLeft,
    ModalContentLineHeight: ModalContentLineHeight,
    ModalContentFontWeight: ModalContentFontWeight,
    ModalContentFontSize: ModalContentFontSize,
    ModalContentColor: ModalContentColor
  }, children))) : null);
};

var _default = Modal;
exports.default = _default;